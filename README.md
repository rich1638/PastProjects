    Effects of Caveola(MATLAB & LaTex):
We developed a mathematical model of the cardiac action potential
in the sinoatrial node cells which include the effect of caveolae
with stochastic opening times. The effects of the caveolae can be
model through probability density functions. These probability 
density functions allow us to model the randomness of the opening
and closing of the thousands of caveolae formed around the cell. 
Lastly, these results are compared with those of a previous 
mathematical model of stochastic caveolae opening development in 
a cardiomyocytes cell. You can find our background paper inside of
the folder labeled Effects of Caveola.

    Ladon-Games(Python):
Simple Arcade style game, which has a universal menu screen,
and pause menu. Games include Snake, Cups, and Pong. Ladon-Games
was written in Python using PyGames. I made this project before I took Object
Oriented design, so the design itself is fairly terrible. This project is mostly
to show I have experience with Python.

    ServeUs(C# & AWS):

For project code please visit: https://gitlab.com/serveus/serveus

ServeUs Project Abstract:
    "There are over a million non-profit organizations registered in the
United States, yet there has been a constant decline in volunteer rates
over the past decade. ServeUs is a social networking platform with a
goal of bridging the widening gap between individuals and non-profits.
By using ServeUs, individuals are paired with causes that they care
about, allowing anyone to easily volunteer their time. Through the latest
cloud technology and advanced software design, ServeUs provides users
with a way to not only connect with organizations, but also to navigate
additional needs for their civic engagement. ServeUs offers the user
up-to-date information on volunteer events and nonprofits in their area,
and retains a detailed record of the user’s activities. Individuals are able
to create their own events for the community to take part in. Developed
in C# with Xamarin, and connected through Amazon Web Services,
ServeUs will be available on both Android and iPhone devices, from
anywhere with an internet connection. ServeUs provides an intuitive,
easy-to-use platform that connects users to many different service
opportunities around the country."

ServeUs is my senior project that I have been working on this year with a team 
consisting of 2 other peers in a agile environment using ScrumDesk to manage 
sprints. ServeUs uses the AWS platform to fulfill a majority of our needs. 
Some of the AWS tools we have been using are Cognito for logins, RDS to host and
manage our SQL database, and Elastic Beanstalk for our restful API which uses 
ASP.NET with MVC. On the code side, the mobile app is built in Xamarin with a 
.Net Core for cross platform capabilities. We follow a strict MVVM pattern for 
the front end with ViewModel first navigation. Lastly we use multi threading in 
terms of asynchronous events.

For more information feel free to contact me.

    Numbers (Java)
A simple server side application which recieves and deduplicates input from 
5 concurrent clients. This project was from a take home coding challenge 
I recieved from one of my past interviews.

    Banking System (C++)
A object oriented C++ based project to represent a banking system. 
Functionality of the project is to provide a saving, and checking account to 
user. Each account is affected differently in terms of monthly interest, fees, 
and minimum account balances. Uses a variety of design 
patterns(singleton, template, etc). Lastly, the banking system allows withdraws,
deposits, and handles different types of currency through a singleton which acts
as a currency converter. If you wish to see this 
project(either the UML, and/or code) please contact me. Because this was a 
project I worked on for a class we are asked not to make this project public.