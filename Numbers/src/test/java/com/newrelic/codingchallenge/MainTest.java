/*
Name   : Jacob Richards
For    : NewRelic
File   : MainTest.java
Date   : 5/13/2018
Project: Data Service Code Challenge
Purpose: Hopefully get me a job...
 */
package com.newrelic.codingchallenge;

import org.junit.Test;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.util.SplittableRandom;
import java.util.concurrent.TimeUnit;

/*
Class  :  MainTest
Purpose:  Main test which opens client side and just uses a splittable random
          number generator to send a infinite loop of doubles. This test has
          no stop sequence. So if you launch this and want to quit out you
          will need to launch ClientTestTwo which sends out the "terminate"
          string. I kept the TimeUnit in there in case while you are testing
          you want to do that. If that is the case you will need to undo the
          comments by the catch statement.

          With 5 MainTests going I was able to maintain about
          4M numbers per 10 second reporting period on my school
          Windows Desktop (e.g: 16 Gib of RAM and 3.3 Ghz Intel i7-5820K processor)
 */
public class MainTest
{
    @Test
    public void test()
    {
      boolean result = true;
      try
      {
        // Create a connection to the server socket on the server application
        InetAddress host = InetAddress.getLocalHost();
        Socket socket = new Socket(host.getHostName(), 4000);


        // Read and display the response message sent by server application
        BufferedReader ois = new BufferedReader(new InputStreamReader (socket.getInputStream ()));
        String message = ois.readLine ();
        System.out.println("Message: " + message);

        // Send a message to the client application
        PrintWriter oos = new PrintWriter(socket.getOutputStream(), true);

        SplittableRandom numGenerator = new SplittableRandom ();

        while (result)
        {
          oos.println( Math.round (numGenerator.nextDouble (100000000, 999999999)));
        //TimeUnit.NANOSECONDS.sleep (100);
        }

        ois.close();
        oos.close();
        socket.close();

      } catch (IOException e)//| InterruptedException e)
      {
        e.printStackTrace();
      }
    }
}