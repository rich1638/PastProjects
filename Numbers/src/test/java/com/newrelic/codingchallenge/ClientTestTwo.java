/*
Name   : Jacob Richards
For    : NewRelic
File   : ClientTestTwo.java
Date   : 5/13/2018
Project: Data Service Code Challenge
Purpose: Hopefully get me a job...
 */
package com.newrelic.codingchallenge;

import org.junit.Test;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.util.SplittableRandom;

/*
Class  :  ClientTestTwo
Purpose:  This class was mainly used to test format flag was working, along with
          terminate, and leading zeros. If you want to test bad data you can
          uncomment "badline" and the server will only read in three values from
          this client. Because after the bad data it disconnects from this specific
          client.
 */
public class ClientTestTwo
{
  @Test
  public void test()
  {

    long test = 0;
    boolean result = true;
    try
    {
      // Create a connection to the server socket on the server application
      InetAddress host = InetAddress.getLocalHost();
      Socket socket = new Socket(host.getHostName(), 4000);


      // Read and display the response message sent by server application
      BufferedReader ois = new BufferedReader(new InputStreamReader (socket.getInputStream ()));
      String message = ois.readLine ();
      System.out.println("Message: " + message);

      // Send a message to the client application
      PrintWriter oos = new PrintWriter(socket.getOutputStream(), true);

      SplittableRandom numGenerator = new SplittableRandom ();

      oos.println("000008000");
      oos.println("000007000");
      oos.println("000006000");
      //oos.println("badline");
      oos.println("000005000");
      oos.println("000004000");
      oos.println("000003000");
      oos.println("000003000");
      oos.println("terminate");


      ois.close();
      oos.close();
      socket.close();

    } catch (IOException  e)
    {
      e.printStackTrace();
    }
  }
}
