/*
Name   : Jacob Richards
For    : NewRelic
File   : DoubleSet.java
Date   : 5/13/2018
Project: Data Service Code Challenge
Purpose: Hopefully get me a job...
 */
package com.newrelic.codingchallenge;

import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

/*
Class  :  DoubleSet
Purpose:  DataStructure to manage our data we are manipulating and outputing
 */
public class DoubleSet
{
  //main fields which store all the unique numbers. I know I didn't need to store
  //them in a set though thought I would in order to make the class more functional
  //for future use. I used AtomicLongs in order to help with concurrency issues
  private volatile Set<Double> doubleSet;
  private volatile AtomicLong unique;
  private volatile AtomicLong duplicates;

  //the printer I passed in
  private PrintWriter logWriter;

  //flags that I pass back to the thread once it tries to add the long to the set
  //CONTINUE - input was good, and thread can continue reading from client
  //TERMINATE - input was "terminate"
  //ERRORSHUTDOWN - misformated data so thread will close connection with that client
  public final int CONTINUE = 0;
  public final int TERMINATE = 1;
  public final int ERRORSHUTDOWN = 2;

  /*
  Constructor  : DoubleSet()
  Purpose      : Constructor
  Arguments    : writer - the printWriter we are passing in
 */
  DoubleSet(PrintWriter writer)
  {
    doubleSet = new HashSet<Double> ();
    unique = new AtomicLong (0);
    duplicates = new AtomicLong (0);
    logWriter = writer;
  }

  /*
  Method    : getDuplicates
  purpose   : returns a copy of Duplicate count
 */
  public AtomicLong getDuplicates ()
  {
    AtomicLong returnedValue = new AtomicLong (0);

    returnedValue.set (duplicates.get ());

    return returnedValue;
  }

  /*
  Method    : getUnique
  purpose   : returns a copy of unique count
  */
  public AtomicLong getUnique ()
  {
    AtomicLong returnedValue = new AtomicLong (0);

    returnedValue.set (unique.get ());

    return returnedValue;
  }

  /*
  Method    : incrementUnique
  purpose   : increments the unique count atomically
  */
  private void incrementUnique()
  {
    unique.getAndIncrement ();
  }

  /*
  Method    : incrementDup
  purpose   : increments the duplicate count atomically
  */
  private void incrementDup()
  {
    duplicates.getAndIncrement ();
  }

  /*
  Method    : getSize
  purpose   : returns the size of our set of uniques
  */
  public long getSize()
  {
    return (long) doubleSet.size ();
  }

  /*
  Method    : resetUni
  purpose   : reset unique count to 0
  */
  public void resetUni()
  {
    duplicates.set (0);
  }

  /*
  Method    : resetDup
  purpose   : reset duplicate count to 0
  */
  public void resetDup()
  {
    unique.set (0);
  }

  /*
  Method    : addSet
  purpose   : this will check a string and add it to the set if it is applicable.
              otherwise if it is the wrong length, not the right format, or
              a terminate string it return a flag to our clientconnections class
  arguments : input - the input string from client
  */
  public int addSet(String input)
  {
    //First thine I check for is size, it if is not the right size I automatically
    //know it is the wrong format and will send a ERRORSHUTDOWN flag which will
    //disconnect the connection with the client who sent that message.
    if(input.length () == 9)
    {
      //check to see if it is a terminate call by the user
      if(input.equals ("terminate"))
      {
        return TERMINATE;
      }
      else
      {
        //this try catch will check to see if the string is actually a number
        //by using a NumberFormatException
        try
        {
          Double fakeNumber  = Double.parseDouble (input);
          //Synchronized this because double set is shared, and log writer is
          //shared. And I don't want multiple threads trying to add to the set
          //at the exact same time, or write to the file at the exact same time.
          synchronized (this)
          {
            if(doubleSet.contains (fakeNumber))
            {
              incrementDup();
            }
            else
            {
              doubleSet.add (Double.parseDouble (input));
              logWriter.println(input);
              incrementUnique();
            }
          }
          return CONTINUE;
        }
        catch (NumberFormatException e)
        {
          return ERRORSHUTDOWN;
        }
      }
    }
    else
    {
      return ERRORSHUTDOWN;
    }
  }
}
