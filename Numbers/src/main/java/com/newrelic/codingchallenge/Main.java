/*
Name   : Jacob Richards
File   : Main.java
For    : NewRelic
Date   : 5/13/2018
Project: Data Service Code Challenge
Purpose: Hopefully get me a job...
 */
package com.newrelic.codingchallenge;

import java.io.*;
import java.net.*;
import java.util.concurrent.*;

/*
Class  :  Main
Purpose:  Serves as the main instance for our server-side application
 */
public class Main
{
  //Server side socket where we will be connecting to the TCP/IP port 4000
  private ServerSocket server;

  //Executor Services we decided to use.
  //  executor  - the thread pool to manage our 5 concurrent clients
  //  scheduler - the service which uses a single thread to run a task to print
  //out a report to system out every 10 seconds.
  private final ExecutorService executor;
  private final ScheduledExecutorService scheduler;

  //  Created a class DoubleSet to manage our data and methods associated with
  //the data. Made Volatile since multiple threads will be accessing it, and
  //needing to know what the last write value was.
  private volatile DoubleSet set;
  //  simple printWriter to open new log file, and write to it. Future plan
  //would be to create a class for file output
  private PrintWriter writer;

  private final int PORT = 4000;
  private final int  MAX_THREADS = 5;

  /*
  Constructor  : Main()
  Purpose      : Default Constructor
   */
  private Main() throws FileNotFoundException, UnsupportedEncodingException
  {
    //initializing our executor services, and printWriter
    executor = Executors.newFixedThreadPool (MAX_THREADS);
    scheduler = Executors.newScheduledThreadPool(1);
    writer = new PrintWriter("numbers.log", "UTF-8");

    //I am passing in the PrintWriter specifically to set because this class
    //contains the method which will be doing the writing.
    set = new DoubleSet (writer);

    //initializing our serverSocket to our port that we will be listening to
    try
    {
      server = new ServerSocket(PORT);
    } catch (IOException e)
    {
      e.printStackTrace();
    }
  }

  /*
  Method    : main
  purpose   : The main thread running our application.
  Arguments : String[] args - command line arguments passed in
   */
  public static void main(String[] args) throws FileNotFoundException,
      UnsupportedEncodingException
  {
    //a warm welcoming for client side
    System.out.println ("Starting up server ....");
    System.out.println ("Listening to port 4000");
    System.out.println ("Waiting for clients...");

    //me trying to get brownie points
    Main NewRelicRocks = new Main ();

    //  One of the assumptions I made was that the schedule thread started at the
    //beginning of the application launch. So it will report "Received 0 unique..
    NewRelicRocks.scheduler.scheduleAtFixedRate(new timingReport(NewRelicRocks.set),
        10, 10 , TimeUnit.SECONDS);

    //method for handling client side connection to our TCP port
    NewRelicRocks.handleConnection ();

    //closing off the file and ending program
    NewRelicRocks.writer.close();
    System.exit(0);
  }

  /*
  Method    : handleConnection
  purpose   : handles the connection being made onto the port, by executing new task
              through our executor. We only have 5 threads so at most we can
              handle only 5 concurrent users. Though when one task completes, or
              a user is disconnected then our free thread will connect with the
              next user waiting.
 */
  private void handleConnection()
  {
    while (!executor.isShutdown ())
    {
      try
      {
        final Socket connection = server.accept ();
        //In the future I would rather move to a execute structure that uses
        //callable, future type. that way I am not having to pass in
        //so much. Also I would want to create a class specifically for the
        //thread arguments I'm passing in to make it a little more clean.
        executor.execute (new ClientConnections (server, connection, executor, set));
      }
      catch (IOException | RejectedExecutionException e)
      {
          System.out.println ("Program Terminated");
      }
    }
    //Terminate has been called so everything is shutting down. I went with
    //shutdownNow because I am assuming that when terminate is called it is
    //wanting the executor to force disconnect out of the current tasks/connections
    executor.shutdownNow ();
    scheduler.shutdownNow ();
  }
}

