/*
Name   : Jacob Richards
For    : NewRelic
File   : ClientConnections.java
Date   : 5/13/2018
Project: Data Service Code Challenge
Purpose: Hopefully get me a job...
 */
package com.newrelic.codingchallenge;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;

/*
Class  :  ClientConnections
Purpose:  task which runs the connection between client and server
 */
public class ClientConnections implements Runnable
{
  private volatile Socket socket;
  private volatile ExecutorService executor;
  private volatile DoubleSet dataSet;
  private volatile int status;
  private volatile ServerSocket server;

  public final int CONTINUE = 0;
  public final int TERMINATE = 1;

  /*
  Constructor  : ClientConnections
  Purpose      : Constructor
  */
  ClientConnections (ServerSocket serverPassed, Socket socketPassed,
      ExecutorService passedExecutor, DoubleSet data)
  {
    socket = socketPassed;
    executor = passedExecutor;
    dataSet = data;
    status = CONTINUE;
    server = serverPassed;
  }

  /*
  Method    : run
  purpose   : instructions for the connection between client and server.
              Assumption I made was when terminate was called you wanted a
              quick shutdown.
  */
  @Override
  public void run ()
  {
    try
    {
      System.out.println("Connected to client");

      // Send a response information to the client application
      PrintWriter oos = new PrintWriter(socket.getOutputStream(), true);
      oos.println ("You are connected :)");

      // Read a message sent by client application
      BufferedReader ois = new BufferedReader(new InputStreamReader (socket.getInputStream ()));
      String message = ois.readLine ();

      //reading the responses from the client
      while(status == CONTINUE && message != null && !executor.isShutdown ())
      {
        //get the status of the last input string our client sent us
        status = dataSet.addSet (message);
        message = ois.readLine ();
      }

      ois.close();
      oos.close ();
      socket.close ();
      System.out.println("Disconnected from client");

      //This is the difference between a client who sends "terminate" and a client
      //who has bad input. If they have bad input the thread will skip over
      //this condition and just end the connection, where as if the input is
      //"terminate" then it will kill the connection between the server and the
      //client.
      if(status == TERMINATE)
      {
        executor.shutdownNow ();
        server.close ();
        //I am curious what the best solution would be to shutdown your executor
        //and not get caught on the main thread waiting for socket accept. I feel
        //closing the server at this point isn't the greatest solution since other
        //threads might still have some input queued up that they are needing to
        //read and that the client has already sent.
      }
    }
    catch (IOException e)
    {
      e.printStackTrace ();
    }
  }
}
