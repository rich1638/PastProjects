/*
Name   : Jacob Richards
For    : NewRelic
File   : timiongReport.java
Date   : 5/13/2018
Project: Data Service Code Challenge
Purpose: Hopefully get me a job...
 */
package com.newrelic.codingchallenge;

/*
Class  :  timingReport
Purpose:  task which runs the data report evey 10 seconds
 */
public class timingReport implements Runnable
{
  private final DoubleSet dataSet;

  /*
  Constructor  : DoubleSet()
  Purpose      : Constructor
  Arguments    : data - the data being passed in
  */
  timingReport (DoubleSet data)
  {
    dataSet = data;
  }

  /*
  Method    : run
  purpose   : prints out to the console the number of new uniques, new duplicates
              and the total uniques every 10 seconds.
  */
  @Override
  public void run ()
  {
    System.out.println("Received " + dataSet.getUnique () + " unique numbers, "
        + dataSet.getDuplicates () + " duplicates. Unique total: "
        + dataSet.getSize ());
    dataSet.resetDup ();
    dataSet.resetUni ();
  }
}
